<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Credits</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('css/bootstrap.css') }}>
    <link rel="stylesheet" href={{ asset('vendors/bootstrap-icons/bootstrap-icons.css') }}>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <link rel="shortcut icon" href={{ asset('/images/logo/logo-2.png') }} type="image/x-icon">
</head>

<body>
    <nav class="navbar navbar-light">
        <div class="container d-block">
            <a href={{url()->previous()}}>
                <i class="bi bi-chevron-left"></i>
                <big>Kembali</big>
            </a>
        </div>
    </nav>


    <div class="container">
        <div class="row">
            <center>
                <div class="card">
                    <div class="card-body">
                        <h1>Credits</h1>
                        <p>
                            Dalam pembuatan aplikasi website ini, kami menggunakan framework <a href="https://laravel.com/" target="_blank">Laravel</a>
                            dan <a href="https://getbootstrap.com/" target="_blank">Bootstrap</a>.<br>
                            Kami juga menggunakan Template Mazer dari <a href="https://github.com/zuramai" target="_blank">Ahmad Saugi</a> (<a href="https://github.com/zuramai" target="_blank">@zuramai</a> on GitHub).
                            
                            <div class="mt-3">
                                Aplikasi website ini dibuat oleh Kelompok 6 dari SMK Telkom Purwokerto,<br>yang beranggotakan :
                            </div><br>
                            <div>
                                <h5>Kareena Widagdo</h5>
                                <small><a href="https://github.com/kareenawidagdo" target="_blank">GitHub</a> &ensp; <a href="mailto:widagdo.kareena@gmail.com" target="_blank">Email</a></small><br><br>
                            </div>
                            <div>
                                <h5>Regina Permatasari</h5>
                                <small><a href="https://github.com/Regina-Permatasari" target="_blank">GitHub</a> &ensp; <a href="mailto:reginapermata06@gmail.com" target="_blank">Email</a></small><br><br>
                            </div>
                            <div>
                                <h5>Satria Duta Praja</h5>
                                <small><a href="https://github.com/SatriaPraja" target="_blank">GitHub</a> &ensp; <a href="mailto:satriadutapc@gmail.com" target="_blank">Email</a></small><br><br>
                            </div>
                            <div>
                                <h5>Bagas Makhali</h5>
                                <small><a href="http://makhalibagas.me/" target="_blank">GitHub</a> &ensp; <a href="mailto:makhalibagas1@gmail.com" target="_blank">Email</a></small><br><br>
                            </div>
                            <div>
                                <h5>Muhammad Royan Nur Ramadhan</h5>
                                <small><a href="https://github.com/muhammadroyannr" target="_blank">GitHub</a> &ensp; <a href="mailto:muhammadroyannr@gmail.com" target="_blank">Email</a></small><br>
                            </div>
                        </p>
                    </div>
                </div>
            </center>
        </div>
    </div>


</body>
</html>