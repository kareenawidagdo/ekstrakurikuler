<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('css/bootstrap.css') }}>

    <link rel="stylesheet" href={{ asset('vendors/perfect-scrollbar/perfect-scrollbar.css') }}>
    <link rel="stylesheet" href={{ asset('vendors/bootstrap-icons/bootstrap-icons.css') }}>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <link rel="shortcut icon" href={{ asset('/images/logo/logo-2.png') }} type="image/x-icon">
    @yield('head')
</head>

<body>
    <div id="app">
        <div id="sidebar">
            <div class="sidebar-wrapper ">
                <div class="sidebar-header">
                    <div class="justify-content-between">
                        <center>
                            <a href="/siswa/profile/{{$data_user->id}}">
                                <img src={{ asset('images/'.$data_user->photo) }} alt="Profile photo" style="width: 9rem; height: 9rem; object-fit:cover; border-radius: 100%;">
                            </a>
                            <h4 class="mt-3">Hai, {{strtok($data_user->name, ' ')}}!</h4>
                            <p style="font-size: 10pt;">{{$data_user->email}}</p>
                        </center>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">Menu</li>

                        <li class="sidebar-item @yield('beranda')">
                            <a href="/siswa/beranda/{{$data_user->id}}" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Beranda</span>
                            </a>
                        </li>

                        <li class="sidebar-item @yield('ekskul')">
                            <a href="/siswa/ekstrakurikuler/{{$data_user->id}}" class='sidebar-link'>
                                <i class="bi bi-ui-radios"></i>
                                <span>Ekstrakurikuler</span>
                            </a>
                        </li>

                        <li class="sidebar-item @yield('profil')">
                            <a href="/siswa/profile/{{$data_user->id}}" class='sidebar-link'>
                                <i class="bi bi-person-fill"></i>
                                <span>Profil Saya</span>
                            </a>
                        </li>

                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>
        <div id="main" class='layout-navbar'>
            <header>
                <nav class="navbar navbar-expand navbar-light ">
                    <div class="container-fluid">
                        <a href="#" class="burger-btn d-block">
                            <i class="bi bi-justify fs-3"></i>
                        </a>

                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                                <li class="nav-item dropdown me-1">
                                    <a class="nav-link active dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                        <i class='bi bi-envelope bi-sub fs-4 text-gray-600'></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                        <li>
                                            <h6 class="dropdown-header">Mail</h6>
                                        </li>
                                        <li><a class="dropdown-item" href="#">No new mail</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown me-3">
                                    <a class="nav-link active dropdown-toggle" href="#" data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                        <i class='bi bi-bell bi-sub fs-4 text-gray-600'></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                        <li>
                                            <h6 class="dropdown-header">Notifications</h6>
                                        </li>
                                        <li><a class="dropdown-item">No notification available</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <div class="dropdown">
                                <a data-bs-toggle="dropdown" aria-expanded="false">
                                    <div class="user-menu d-flex">
                                        <div class="user-name text-end me-3">
                                            <h6 class="mb-0 text-gray-600">{{$data_user->name}}</h6>
                                            <p class="mb-0 text-sm text-gray-600">{{$data_user->class}}</p>
                                        </div>
                                        <div class="user-img d-flex align-items-center">
                                            <div class="avatar avatar-md">
                                                <img src={{ asset('images/'.$data_user->photo) }} alt="Profile photo" style="object-fit: cover;">
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                    <li>
                                        <h6 class="dropdown-header">Halo, {{strtok($data_user->name, ' ')}}!</h6>
                                    </li>
                                    <li><a class="dropdown-item" href="/siswa/profile/{{$data_user->id}}"><i class="icon-mid bi bi-person me-2"></i> Profil Saya</a></li>
                                    <li><a class="dropdown-item" href="/siswa/ekstrakurikuler/{{$data_user->id}}"><i class="icon-mid bi bi-clipboard me-2"></i>
                                            Ekstrakurikuler</a></li>
                                    <li><a class="dropdown-item" href="/siswa/bantuan/{{$data_user->id}}"><i class="icon-mid bi bi-info-circle me-2"></i>
                                            Bantuan</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="/logout"><i
                                                class="icon-mid bi bi-box-arrow-left me-2"></i> Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
            <div id="main-content">
                <div class="page-heading">
                    @yield('konten')
                </div>

                <footer>
                    <div class="footer clearfix mb-0 text-muted">
                        <div class="float-start">
                            <p>2021 &copy; SMA JAC School</p>
                        </div>
                        <div class="float-end">
                            Created by <a href="/credits"><b>KRSBR</b></a>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <script src={{ asset('vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}></script>
    <script src={{ asset('js/bootstrap.bundle.min.js') }}></script>
    <script src={{ asset('js/main.js') }}></script>
</body>
</html>