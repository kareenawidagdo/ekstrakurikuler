<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/bootstrap-icons/bootstrap-icons.css')}}">
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <link rel="shortcut icon" href={{ asset('/images/logo/logo-2.png') }} type="image/x-icon">
    <!-- <link rel="stylesheet" href="assets/css/pages/auth.css"> -->
</head>
<body align="center">
    <div id="auth">
        <div class="container">
        <div class="row h-50">
        <div class="col-md-4"></div>
            <div class="col-md-4" style="margin: 27vh 0;">
                <div id="auth-left">
                    <h1 class="auth-title mb-4">Login</h1>
                    <form action="/auth-login" method="POST">
						@csrf
                        <div class="form-group position-relative has-icon-left mb-4">
                            <input type="text" name="email" class="form-control form-control-xl" placeholder="Username">
                            <div class="form-control-icon">
                                <i class="bi bi-person"></i>
                            </div>
                        </div>
                        <div class="form-group position-relative has-icon-left mb-4">
                            <input type="password" name="password" class="form-control form-control-xl" placeholder="Password">
                            <div class="form-control-icon">
                                <i class="bi bi-shield-lock"></i>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block btn-lg shadow-lg mt-3">LOGIN</button>
                    </form>
                </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>

<!--===============================================================================================-->
	<script src={{asset('vendor/jquery/jquery-3.2.1.min.js')}}></script>
<!--===============================================================================================-->
	<script src={{asset('vendor/bootstrap/js/popper.js')}}></script>
	<script src={{asset('vendor/bootstrap/js/bootstrap.min.js')}}></script>
<!--===============================================================================================-->
	<script src={{asset('vendor/select2/select2.min.js')}}></script>
<!--===============================================================================================-->
	<script src={{asset('vendor/tilt/tilt.jquery.min.js')}}></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src={{asset('js/main.js')}}></script>
</body>
</html>
    