<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('/css/bootstrap.css') }}>
    <link rel="stylesheet" href={{ asset('/vendors/iconly/bold.css') }}>
    <link rel="stylesheet" href={{ asset('/vendors/perfect-scrollbar/perfect-scrollbar.css') }}>
    <link rel="stylesheet" href={{ asset('/vendors/bootstrap-icons/bootstrap-icons.css') }}>
    <link rel="stylesheet" href={{ asset('/css/app.css') }}>
    <link rel="shortcut icon" href={{ asset('/images/logo/logo-2.png') }} type="image/x-icon">
</head>

<body>
    <div id="app">
        <div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="justify-content-between">
                        <center>
                            <a href="/admin/beranda"><img src={{asset("images/logo/logo-1.png")}} alt="JAC SCHOOL" class="img-fluid" style="object-fit: cover;width: 12rem;height: 8rem;"></a>
                        </center>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">Menu</li>

                        <li class="sidebar-item @yield('beranda')">
                            <a href="/admin/beranda" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Beranda</span>
                            </a>
                        </li>

                        <li class="sidebar-item @yield('ekskul')">
                            <a href="/admin/ekskul" class='sidebar-link'>
                                <i class="bi bi-stack"></i>
                                <span>Ekstrakurikuler</span>
                            </a>
                        </li>

                        <li class="sidebar-item @yield('pembimbing')">
                            <a href="/admin/pembimbing" class='sidebar-link'>
                                <i class="bi bi-collection-fill"></i>
                                <span>Pembimbing</span>
                            </a>
                        </li>

                        <li class="sidebar-item @yield('siswa')">
                            <a href="/admin/siswa" class='sidebar-link'>
                                <i class="bi bi-person-fill"></i>
                                <span>Siswa</span>
                            </a>
                        </li>

                        <li class="sidebar-item mt-4">
                            <a href="/logout" class='hover-shadow'>
                                <button type="button" class="btn btn-danger btn-block" style="border-radius: 7px;">LOGOUT</button>
                            </a>
                        </li>
                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>
        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>
            @yield('konten')
            <footer>
                <div class="footer clearfix mb-0 text-muted">
                    <div class="float-start">
                        <p>2021 &copy; SMA JAC School</p>
                    </div>
                    <div class="float-end">
                        <p>Created by <a href="/credits"><b>KRSBR</b></a></p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="{{ asset('vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

    <script src="{{ asset('vendors/apexcharts/apexcharts.js') }}"></script>
    <script src="{{ asset('js/pages/dashboard.js') }}"></script>

    <script src="{{ asset('js/main.js') }}"></script>
</body>

</html>