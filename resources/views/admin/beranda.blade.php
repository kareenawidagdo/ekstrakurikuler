@extends('layout.admin')

@section('title', 'Beranda Admin SMA JAC School')

@section('beranda','active')

@section('konten')
<section id="modal-themes">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h1>ADMIN SMA JAC SCHOOL</h1>
                    <br>
                    <div class="buttons d-flex justify-content-end">
                    <button type="button" class="btn btn-primary rounded-pill" data-bs-toggle="modal" data-bs-target="#modalEkskul">
                        Tambah
                    </button>
                    </div>
                </div>
            </div> 
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade text-left" id="modalEkskul" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title white" id="myModalLabel160">
                        Pembimbing
                    </h5>
                    <button type="button" class="close"
                        data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <center>
                    Memilih Pembimbing Ekstrakurikuler : <br><br>

                        <button type="button" style="width: 60%;" class="btn btn-success mb-2" data-bs-toggle="modal" data-bs-target="#daftarPembimbing">Daftar Pembimbing</button><br>
                        <a type="button" style="width: 60%;" class="btn btn-success" href="/admin/ekskul/pembimbing-baru">Buat Pembimbing Baru</a><br>
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal - Daftar Pembimbing -->
    <div class="modal fade text-left" id="daftarPembimbing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title white" id="myModalLabel160">
                        Pilih Pembimbing
                    </h5>
                    <button type="button" class="close"
                        data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <center>
                        @foreach ($data_pembimbing as $dp)
                            <a type="button" href="/admin/ekskul/create/{{$dp->id}}" class="btn btn-outline-primary btn-block mb-3">{{$dp->nama}}</a>
                        @endforeach
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Ekstrakurikuler -->
    <div class="row">
        @foreach ($data_ekskul as $row)
            <div class="col-12 col-md-6 order-md-1">
                <div class="card">
                    <div class="card-body">
                        <h2>{{$row->nama_ekskul}}</h2>
                        <hr>
                        <div class="buttons d-flex justify-content-end">
                            <a type="button" href="/admin/ekskul/edit/{{$row->id}}" class="btn btn-secondary btn-sm">Edit</a>
                            <button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#modalHapus-{{$row->id}}">
                                Hapus
                            </button>
                        </div>
                    </div>
                </div> 
            </div>
        @endforeach
    </div>
</section>

@foreach($data_ekskul as $de)
<div class="modal modal-borderless fade" id="modalHapus-{{$de->id}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">
                    Yakin Anda ingin menghapus?
                </h5>
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-footer">
                <a href="/admin/ekskul/delete/{{$de->id}}" type="button" class="btn btn-danger">
                    Ya
                </a>
                <button type="button" class="btn btn-light-secondary ml-1"
                    data-bs-dismiss="modal">
                    Tidak
                </button>
            </div>
        </div>
    </div>
</div>
@endforeach

@stop

