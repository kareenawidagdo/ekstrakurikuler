@extends('layout.user')

@section('title')
    Bantuan
@endsection

@section('beranda','active')

@section('konten')
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-body">
                    <center>
                        <h1>Bantuan</h1>
                        <p>
                            Jika Anda  mengalami kendala atau membutuhkan bantuan, silahkan hubungi Admin kami pada link berikut.
                        </p>
                        <table class="table-responsive mb-4" cellpadding="5">
                            <tr>
                                <td width="30"><i class="bi bi-whatsapp"></i></td>
                                <td>
                                    <a href="https://api.whatsapp.com/send?phone=6282242044001" target="_blank">+62 82242024001</a>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="bi bi-envelope"></i></td>
                                <td>
                                    <a href="mailto:admin@jacschool.com" target="_blank">admin@jacschool.com</a>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="bi bi-instagram"></i></td>
                                <td>
                                    <a href="https://www.instagram.com/jacschool.official/" target="_blank">@smajacschool</a>
                                </td>
                            </tr>
                            <tr>
                                <td><i class="bi bi-twitter"></i></td>
                                <td>
                                    <a href="https://twitter.com/jacschool" target="_blank">@smajacschool</a>
                                </td>
                            </tr>
                        </table>
                        <p>
                            <div class="col-12 col-md-4 order-md-2">
                                <a type="button" class="btn btn-outline-danger btn-block" href="/siswa/beranda/{{$data_user->id}}">Kembali ke Beranda</a>
                            </div>
                        </p>
                    </center>
                </div>
            </div>
        </div>
    </div>
@endsection