@extends('layout.user')

@section('title')
    {{$data_ekskul->nama_ekskul}}
@endsection

@section('beranda','active')

@section('konten')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="row mt-2">
                <div class="col-12 col-md-6 order-md-1 order-first">
                    <center>
                        <img src={{ asset('images/'.$data_ekskul->foto) }} alt="Foto {{$data_ekskul->nama_ekskul}}" width="450" class="img-fluid" style="border: 10px solid white;box-shadow: 0px 1px 35px 0px rgba(0, 0, 0, 0.3);">
                    </center>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-last">
                    <h2 class="mt-3">{{$data_ekskul->nama_ekskul}}</h2>
                    <p>{{$data_ekskul->deskripsi}}</p>
                    <h5>Nama Pembimbing</h5>
                    <p>{{$data_ekskul->nama}}</p>
                    <h5>Jadwal {{$data_ekskul->nama_ekskul}}</h5>
                    <p>Hari {{$data_ekskul->hari}}, mulai pukul {{\Carbon\Carbon::parse($data_ekskul->jam)->format('H:i')}}</p>
                    <h5>Tempat</h5>
                    <p>{{$data_ekskul->tempat}}</p>
                    <h5>Media Grup <span style="color: red;">*</span></h5>
                    <p>{{$data_ekskul->media}}</p>
                    {{-- <a type="button" class="btn btn-outline-danger btn-block" href="/siswa/join/{{$data_ekskul->slug}}/{{$data_user->id}}"></a> --}}
                    <button type="button" class="btn btn-outline-danger btn-block" data-bs-toggle="modal" data-bs-target="#joinEkskul">
                        Ikut Bergabung
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-borderless fade text-left" id="joinEkskul" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel19" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-sm"
        role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #E5E5E5;">
                <h4 class="modal-title" id="myModalLabel19">Bergabung di <br><big style="font-size: 18pt;">{{$data_ekskul->nama_ekskul}}</big></h4>
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <form action="/siswa/join/{{$data_ekskul->id}}/{{$data_user->id}}" method="POST">
                @csrf
                <div class="modal-body">
                    <p>Anda hanya perlu memasukkan No HP/WhatsApp</p>
                    <h6>No HP</h6>
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="basic-addon1">+62</span>
                        <input type="text" name="hp" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-secondary btn-sm"
                        data-bs-dismiss="modal">
                        Tutup
                    </button>
                    <button type="submit" class="btn btn-primary ml-1 btn-sm">
                        Gabung
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection