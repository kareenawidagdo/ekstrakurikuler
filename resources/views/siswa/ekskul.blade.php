@extends('layout.user')

@section('title')
    Ekstrakurikuler
@stop

@section('ekskul','active')

@section('konten')
<div class="container">
    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12">
                    <h1>Ekstrakurikuler</h1>
                    <p>Ekstrakurikuler yang Anda Ikuti</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Ekstrakurikuler -->
<section id="content-types">
    <div class="container">
        <div class="row">
            @foreach ($data_ekskul as $row)
                <div class="col-12 col-md-6 order-md-1">
                    <div class="card">
                        <div class="card-content">
                            <img class="card-img-top img-fluid" src={{ asset('images/'.$row->foto) }} alt="Foto {{$row->nama_ekskul}}" style="object-fit: cover;height: 15rem;"/>
                            <div class="card-body">
                                <h4>{{$row->nama_ekskul}}</h4>
                                <p class="card-text">
                                    {{ Str::limit($row->deskripsi, 150) }}
                                </p>
                                <a type="button" class="btn btn-light-secondary" href="/siswa/ekstrakurikuler/{{$row->slug}}/{{$data_user->id}}">Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

@stop