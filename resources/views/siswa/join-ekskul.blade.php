@extends('layout.user')

@section('title')
    Telah bergabung di {{$data_ekskul->nama_ekskul}}
@endsection

@section('beranda','active')

@section('konten')
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-body">
                    <center>
                        <h1>Selamat Bergabung!</h1>
                        <hr>
                        <p>
                            Terima kasih telah bergabung di ekstrakurikuler {{$data_ekskul->nama_ekskul}}. Selanjutnya, Anda akan 
                            dibimbing oleh {{$data_ekskul->nama}}. <br>
                            Silahkan segera bergabung ke grup {{$data_ekskul->nama_ekskul}} pada link berikut.
                        </p>
                        <br>
                        <h5>{{$data_ekskul->media}}</h5>
                        <a href="{{$data_ekskul->link_grup}}" target="_blank">Link Gabung Grup</a>
                        <br><br>
                        <p>
                            <div class="col-12 col-md-4 order-md-2">
                                <a type="button" class="btn btn-outline-danger btn-block" href="/siswa/beranda/{{$data_user->id}}">Kembali ke Beranda</a>
                            </div>
                        </p>
                    </center>
                </div>
            </div>
        </div>
    </div>
@endsection