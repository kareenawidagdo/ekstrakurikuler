@extends('layout.user')

@section('title')
    {{$data_ekskul->nama_ekskul}}
@endsection

@section('ekskul','active')

@section('konten')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div class="row mt-2">
                <div class="col-12 col-md-6 order-md-1 order-first">
                    <center>
                        <img src={{ asset('images/'.$data_ekskul->foto) }} alt="Foto {{$data_ekskul->nama_ekskul}}" width="450" class="img-fluid" style="border: 10px solid white;box-shadow: 0px 1px 35px 0px rgba(0, 0, 0, 0.3);">
                    </center>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-last">
                    <h2 class="mt-3">{{$data_ekskul->nama_ekskul}}</h2>
                    <p>{{$data_ekskul->deskripsi}}</p>
                    <h5>Nama Pembimbing</h5>
                    <p>{{$data_ekskul->nama}}</p>
                    <h5>Jadwal {{$data_ekskul->nama_ekskul}}</h5>
                    <p>Hari {{$data_ekskul->hari}}, mulai pukul {{\Carbon\Carbon::parse($data_ekskul->jam)->format('H:i')}}</p>
                    <h5>Tempat</h5>
                    <p>{{$data_ekskul->tempat}}</p>
                    <h5>Status</h5>
                    <p><span class="badge bg-light-success">Bergabung</span></p>
                    <div class="row mt-2">
                        <div class="col-12 col-md-6 order-md-1 order-first">
                            <div class="dropdown">
                                <a href="#" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    <button class="btn btn-outline-success btn-block mb-2">
                                        Hubungi Pembimbing
                                    </button>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                                    <li>
                                        <a class="dropdown-item" href="https://api.whatsapp.com/send?phone=62{{$data_ekskul->no_hp}}" target="_blank">via WhatsApp</a>
                                    </li>
                                    <li>
                                        <a href="mailto:{{$data_ekskul->email}}" class="dropdown-item" target="_blank">via Email</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-last">
                            <button type="button" class="btn btn-outline-danger btn-block" data-bs-toggle="modal" data-bs-target="#modalKeluar">
                                Keluar Ekskul
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-borderless fade" id="modalKeluar" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">
                    Yakin Anda ingin keluar dari Ekstrakurikuler "{{$data_ekskul->nama_ekskul}}"?
                </h5>
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-footer">
                <a href="/siswa/keluar/{{$data_ekskul->id}}/{{$data_user->id}}" type="button" class="btn btn-danger">
                    Ya
                </a>
                <button type="button" class="btn btn-light-secondary ml-1"
                    data-bs-dismiss="modal">
                    Tidak
                </button>
            </div>
        </div>
    </div>
</div>
@endsection