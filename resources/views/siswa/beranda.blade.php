@extends('layout.user')

@section('title','Beranda SMA JAC School')

@section('beranda','active')

@section('konten')
<section id="modal-themes">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1>Selamat Datang, {{strtoupper($data_user->name)}} !</h1>
                </div>
            </div> 
        </div>
    </div>

    <!-- Ekstrakurikuler -->
    <div class="row">
        @foreach ($data_ekskul as $row)
            <div class="col-12 col-md-6 order-md-1">
                <div class="card">
                    <div class="card-header">
                        <h2>{{$row->nama_ekskul}}</h2>
                        <hr>
                        <p>{{ Str::limit($row->deskripsi, 150) }}</p>
                        <div class="buttons d-flex justify-content-end">
                        <a href="/siswa/detail/{{$row->slug}}/{{$data_user->id}}" class="btn btn-info rounded-pill">Detail</a>
                        </div>
                    </div>
                </div> 
            </div>
        @endforeach
    </div>
</section>
@endsection