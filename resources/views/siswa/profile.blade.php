@extends('layout.user')

@section('title')
    Profil {{$data_user->name}}
@stop

@section('profil','active')

@section('head')
    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css" rel="stylesheet">
@endsection

@section('konten')
<div class="container">
    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-first">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <a data-bs-toggle="modal" data-bs-target="#modalProfile" style="cursor: pointer;">
                                    <img src={{ asset('images/'.$data_user->photo) }} alt="Profile photo" class="img-fluid hover-shadow" style="object-fit: cover;width: 12rem;height: 12rem;border-radius: 100%;">
                                </a>
                                <h3 class="mt-3">{{$data_user->name}}</h3>
                                <p>{{$data_user->email}}</p>
                                @if($data_user->description != "")
                                    <div class="container mt-4">
                                        <blockquote style="border: 2px solid #e9e9e9;padding:7px;">{{$data_user->description}}</blockquote>
                                    </div>
                                @endif
                            </center>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-last">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#profile"
                                        role="tab" aria-controls="home" aria-selected="true">Edit Profil</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#password"
                                        role="tab" aria-controls="profile" aria-selected="false">Ganti Password</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                    aria-labelledby="home-tab">
                                    <div class="mt-4">
                                        <form action="/siswa/profile/edit/{{$data_user->id}}" method="POST">
                                            @csrf
                                            <h6>Nama</h6>
                                            <input type="text" name="nama" class="form-control" value="{{$data_user->name}}"><br>
                                            <h6>Email</h6>
                                            <input type="text" name="email" class="form-control" value="{{$data_user->email}}" disabled><br>
                                            <h6>Kelas</h6>
                                            <input type="text" name="kelas" class="form-control" value="{{$data_user->class}}" disabled><br>
                                            <h6>Bio</h6>
                                            <textarea name="bio" rows="3" class="form-control">{{$data_user->description}}</textarea><br>
                                            <div class="col-sm-12 d-flex justify-content-end">
                                                <button type="reset" class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                <button type="submit" class="btn btn-primary me-1 mb-1">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="password" role="tabpanel"
                                    aria-labelledby="profile-tab">
                                    <div class="mt-4">
                                        <form action="/siswa/profile/change-password/{{$data_user->id}}" method="POST">
                                            @csrf
                                            <div class="mb-2">Current Password</div>
                                            <input type="password" name="current_password" class="form-control"><br>
                                            <div class="mb-2">New Password</div>
                                            <input type="password" name="password" class="form-control"><br>
                                            <div class="mb-2">Confirm New Password</div>
                                            <input type="password" name="confirm_password" class="form-control"><br>
                                            <div class="col-sm-12 d-flex justify-content-end">
                                                <button type="reset" class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                <button type="submit" class="btn btn-primary me-1 mb-1">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Profile Modal -->
<div class="modal modal-borderless fade" id="modalProfile" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable modal-sm"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    <a href="#lihatProfil" data-bs-toggle="modal" data-bs-target="#lihatProfil">Lihat Foto Profil</a><br><hr>
                    <a href="#gantiProfil" data-bs-toggle="modal" data-bs-target="#gantiProfil">Ganti Foto Profil</a><br><hr>
                    <a href="/siswa/profile/hapus/{{$data_user->id}}">Hapus Foto Profil</a><br>
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary btn-sm"
                    data-bs-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Lihat Profil Modal -->
<div class="modal modal-borderless fade" id="lihatProfil" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-body">
                <center>
                    <img src={{ asset('images/'.$data_user->photo) }} alt="Foto Profil" class="img-fluid" width="100%">
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary btn-sm"
                    data-bs-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Ganti Profil Modal -->
<div class="modal modal-borderless fade" id="gantiProfil" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="gantiProfil">
                    Ganti Foto Profil
                </h5>
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <form action="/siswa/profile/ganti/{{$data_user->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <center>
                        <p style="text-align: left;">Pilih Foto :</p>
                        <input type="file" class="form-control" accept="image/*" name="foto">
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-secondary btn-sm"
                        data-bs-dismiss="modal">
                        Close
                    </button>
                    <input type="submit" class="btn btn-primary ml-1 btn-sm">
                </div>
            </form>
        </div>
    </div>
</div>
@stop