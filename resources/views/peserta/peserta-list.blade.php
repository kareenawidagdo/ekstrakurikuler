@extends('layout.admin')

@section('title', 'Siswa')

@section('siswa','active')

@section('konten')

<div class="page-heading">
<div class="page-title">
    <div class="row">
        <div class="col-12 col-md-3 order-md-1">
            <h3>Daftar Siswa</h3>
            <p class="text-subtitle text-muted">Siswa yang Mengikuti Ekstrakurikuler SMA JAC School</p>
        </div>
        
        <div class="col-12 col-md-6 order-md-2 mb-2">
            <form action="/admin/siswa" method="GET" class="d-flex">
                @csrf
                <input class="form-control me-2" id="myInput" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit" name="btn" value="1">Search</button>
            </form>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script>
            $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
            });
            </script>
        </div>
        <div class="col-12 col-md-3 order-md-3">
            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg float-lg-end">
                <ol class="breadcrumb">
                    <a href="/admin/siswa/create" class="btn btn-primary">Daftar Akun</a>
                </ol>
            </nav>
        </div>
    </div>
</div>
<br>
<section class="section">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Kelas</th>
                            <th style="width:20px;">Ekskul</th>
                        </tr>
                    </thead>
                    <tbody id="myTable">
                        @foreach ($data_user as $row)
                            @if($row->account_status === "2")
                                <tr>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->email}}</td>
                                    <td>{{$row->class}}</td>
                                    <td>
                                        <a href="/admin/siswa/ekskul/{{$row->id}}" class="btn btn-sm btn-secondary">Ekskul</a>
                                    </td>
                                </tr>
                                @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@stop