@extends('layout.admin')

@section('title', 'Buat Akun Siswa')

@section('siswa','active')

@section('konten')
<div class="page-heading">
<div class="page-title">
    <div class="row">
        <div class="col-12 col-md-6 order-md-1 order-last mb-3">
            <h3>Buat Akun Siswa</h3>
        </div>
        <div class="col-12 col-md-6 order-md-2 order-first">
            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                <ol class="breadcrumb">
                </ol>
            </nav>
        </div>
    </div>
</div>

<!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <form action={{ url('admin/siswa/save') }} method="POST" class="form form-horizontal">
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-1">
                                        <center>Nama</center>
                                    </div>
                                    <div class="col-md-11 form-group">
                                        <input type="text" id="first-name" class="form-control" name="name">
                                    </div>
                                    <div class="col-md-1">
                                        <center>Email</center>
                                    </div>
                                    <div class="col-md-11 form-group">
                                        <input type="email" id="email-id" class="form-control" name="email">
                                    </div>
                                    <div class="col-md-1">
                                        <center>Kelas</center>
                                    </div>
                                    <div class="col-md-11 form-group">
                                        <input type="text" class="form-control" name="kelas">
                                    </div>
                                    <div class="col-sm-12 d-flex justify-content-end">
                                        <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Submit</button>
                                        <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                    </div>
                                 </div>
                             </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop