<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Akun Baru Siswa</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href={{ asset('css/bootstrap.css') }}>
    <link rel="stylesheet" href={{ asset('vendors/bootstrap-icons/bootstrap-icons.css') }}>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <link rel="shortcut icon" href={{ asset('/images/logo/logo-2.png') }} type="image/x-icon">
</head>
<body>
    <nav class="navbar navbar-light">
        <div class="container d-block">
            <a href="/admin/siswa">
                <i class="bi bi-chevron-left"></i>
                <big>Kembali</big>
            </a>
        </div>
    </nav>

    <div class="container">
        <center>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card" style="margin:10vh 0;">
                    <div class="card-header">
                        <h1>Akun Baru Siswa</h1>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-lg table-bordered">
                                <tr>
                                    <th>NAMA</th>
                                    <td>{{$data_user->name}}</td>
                                </tr>
                                <tr>
                                    <th>EMAIL</th>
                                    <td>{{$data_user->email}}</td>
                                </tr>
                                <tr>
                                    <th>KELAS</th>
                                    <td>{{$data_user->class}}</td>
                                </tr>
                            </table>
                        </div>
                        <p>* Screenshot halaman ini dan kirimkan ke siswa</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </center>
    </div>
</body>
</html>