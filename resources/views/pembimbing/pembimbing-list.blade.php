@extends('layout.admin')

@section('title', 'Pembimbing')

@section('pembimbing','active')

@section('konten')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-3 order-md-1">
                <h3>Pembimbing</h3>
                <p class="text-subtitle text-muted">Pembimbing Ekstrakurikuler SMA JAC School</p>
            </div>
            <div class="col-12 col-md-6 order-md-2 mb-2">
                <form action="/admin/pembimbing" class="d-flex" method="GET">
                    @csrf
                    <input class="form-control me-2" id="myInput" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit" name="btn" value="1">Search</button>
                </form>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                <script>
                $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
                });
                </script>
            </div>
            <div class="col-12 col-md-3 order-md-3">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <a href="/admin/pembimbing/create" class="btn btn-primary">Tambah Pembimbing</a>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table1">
                        <thead>
                            <tr>
                                <th>Nama Pembimbing</th>
                                <th>Email</th>
                                <th>No HP</th>
                                <th>Ekskul</th>
                                <th style="width: 22px;">Opsi</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            @foreach ($data_pembimbing as $row)
                                <tr>
                                    <td>{{$row->nama}}</td>
                                    <td>{{$row->email}}</td>
                                    <td>+62 {{$row->no_hp}}</td>
                                    <td>
                                        <a href="/admin/pembimbing/ekskul/{{$row->id}}" class="btn btn-sm btn-secondary">Ekskul</a>
                                    </td>
                                    <td>
                                        <a href="/admin/pembimbing/edit/{{$row->id}}" class="btn btn-sm btn-warning btn-block mb-1">Edit</a>
                                        {{-- <a href="/admin/pembimbing/delete/{{$row->id}}" class="btn btn-sm btn-danger">Hapus</a> --}}
                                        <button type="button" class="btn btn-sm btn-danger btn-block" data-bs-toggle="modal" data-bs-target="#modalHapus-{{$row->id}}">
                                            Hapus
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

@foreach($data_pembimbing as $dp)
<div class="modal modal-borderless fade" id="modalHapus-{{$dp->id}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">
                    Yakin Anda ingin menghapus?
                </h5>
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-footer">
                <a href="/admin/pembimbing/delete/{{$dp->id}}" type="button" class="btn btn-danger">
                    Ya
                </a>
                <button type="button" class="btn btn-light-secondary ml-1"
                    data-bs-dismiss="modal">
                    Tidak
                </button>
            </div>
        </div>
    </div>
</div>
@endforeach

@stop