@extends('layout.admin')

@section('title', 'Edit Data Pembimbing')

@section('pembimbing','active')

@section('konten')

<div class="page-heading">
                <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last mb-3">
                            <h3>Edit Data Pembimbing</h3>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-lg col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="/admin/pembimbing/update/{{$data_pembimbing->id}}" method="POST" class="form form-horizontal">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <label>Nama</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" class="form-control"
                                                            name="nama" value="{{$data_pembimbing->nama}}">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Email</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="email" class="form-control"
                                                            name="email" value="{{$data_pembimbing->email}}">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>No HP</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <div class="input-group mb-3">
                                                            <span class="input-group-text" id="basic-addon1">+62</span>
                                                            <input type="text" name="hp" class="form-control" value="{{$data_pembimbing->no_hp}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 d-flex justify-content-end">
                                                        <button type="submit"
                                                            class="btn btn-primary me-1 mb-1">Edit</button>
                                                        <button type="reset"
                                                            class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic Horizontal form layout section end -->
            </div>
@stop