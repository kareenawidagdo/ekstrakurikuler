@extends('layout.admin')

@section('title')
    Ekstrakurikuler - {{$data_pembimbing->nama}}
@stop

@section('pembimbing','active')

@section('konten')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Ekstrakurikuler - {{$data_pembimbing->nama}}</h3>
                <p class="text-subtitle text-muted">Ekstrakurikuler yang Dibimbing oleh {{$data_pembimbing->nama}}</p>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table1">
                        <thead>
                            <tr>
                                <th>Nama Ekstrakurikuler</th>
                                <th>Foto</th>
                                <th>Kategori</th>
                                <th>Hari</th>
                                <th>Jam</th>
                                <th>Tempat</th>
                                <th>Deskripsi</th>
                                <th>Grup</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_ekskul as $row)
                                <tr>
                                    <td>{{$row->nama_ekskul}}</td>
                                    <td>
                                        <img src="{{ asset('images/'.$row->foto) }}" alt="Image" width="300" class="img-fluid">
                                    </td>
                                    <td>{{$row->kategori}}</td>
                                    <td>{{$row->hari}}</td>
                                    <td>{{\Carbon\Carbon::parse($row->jam)->format('H:i')}}</td>
                                    <td>{{$row->tempat}}</td>
                                    <td>{{$row->deskripsi}}</td>
                                    <td><center>{{$row->media}}<br><b><a href="{{$row->link_grup}}" target="_blank">link</a></b></center></td>
                                    <td>
                                        <a href="/admin/ekskul/edit/{{$row->id}}" class="btn btn-sm btn-warning btn-block mb-1">Edit</a>
                                        {{-- <a href="/admin/ekskul/delete/{{$row->id}}" class="btn btn-sm btn-danger btn-block">Hapus</a> --}}
                                        <button type="button" class="btn btn-sm btn-danger btn-block" data-bs-toggle="modal" data-bs-target="#modalHapus-{{$row->id}}">
                                            Hapus
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

@foreach($data_ekskul as $de)
<div class="modal modal-borderless fade" id="modalHapus-{{$de->id}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">
                    Yakin Anda ingin menghapus?
                </h5>
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-footer">
                <a href="/admin/ekskul/delete/{{$de->id}}" type="button" class="btn btn-danger">
                    Ya
                </a>
                <button type="button" class="btn btn-light-secondary ml-1"
                    data-bs-dismiss="modal">
                    Tidak
                </button>
            </div>
        </div>
    </div>
</div>
@endforeach

@stop