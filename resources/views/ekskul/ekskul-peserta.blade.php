@extends('layout.admin')

@section('title')
    Peserta - {{$data_ekskul->nama_ekskul}}
@stop

@section('ekskul','active')

@section('konten')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Peserta - {{$data_ekskul->nama_ekskul}}</h3>
                <p class="text-subtitle text-muted">Peserta Ekstrakurikuler "{{$data_ekskul->nama_ekskul}}"</p>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table1">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Kelas</th>
                                <th>No HP</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_siswa as $row)
                                <tr>
                                    <td>{{$row->nama}}</td>
                                    <td>{{$row->email}}</td>
                                    <td>{{$row->kelas}}</td>
                                    <td>+62 {{$row->no_hp}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-danger btn-block" data-bs-toggle="modal" data-bs-target="#modalHapus-{{$row->id}}">
                                            Hapus
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

@foreach($data_siswa as $ds)
<div class="modal modal-borderless fade" id="modalHapus-{{$ds->id}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">
                    Yakin Anda ingin menghapus?
                </h5>
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-footer">
                <a href="/admin/ekskul/siswa/delete/{{$ds->id}}" type="button" class="btn btn-danger">
                    Ya
                </a>
                <button type="button" class="btn btn-light-secondary ml-1"
                    data-bs-dismiss="modal">
                    Tidak
                </button>
            </div>
        </div>
    </div>
</div>
@endforeach

@stop