@extends('layout.admin')

@section('title', 'Ekstrakurikuler')

@section('ekskul','active')

@section('konten')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-3 order-md-1t">
                <h3>Ektrakurikuler</h3>
                <p class="text-subtitle text-muted">Ektrakurikuler SMA JAC School</p>
            </div>
            <div class="col-12 col-md-6 order-md-2 mb-2">
                <form action="/admin/ekskul" method="GET" class="d-flex">
                    @csrf
                    <input class="form-control me-2" id="myInput" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit" name="btn" value="1">Search</button>
                </form>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                <script>
                $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
                });
                </script>
            </div>
            <div class="col-12 col-md-3 order-md-3">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <button type="button" class="btn btn-primary block" data-bs-toggle="modal" data-bs-target="#modalEkskul">
                            Tambah Ekstrakurikuler
                        </button>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade text-left" id="modalEkskul" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title white" id="myModalLabel160">
                        Pembimbing
                    </h5>
                    <button type="button" class="close"
                        data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <center>
                    Memilih Pembimbing Ekstrakurikuler : <br><br>

                        <button type="button" style="width: 60%;" class="btn btn-success mb-2" data-bs-toggle="modal" data-bs-target="#daftarPembimbing">Daftar Pembimbing</button><br>
                        <a type="button" style="width: 60%;" class="btn btn-success" href="/admin/ekskul/pembimbing-baru">Buat Pembimbing Baru</a><br>
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal - Daftar Pembimbing -->
    <div class="modal fade text-left" id="daftarPembimbing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title white" id="myModalLabel160">
                        Pilih Pembimbing
                    </h5>
                    <button type="button" class="close"
                        data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <center>
                        @foreach ($data_pembimbing as $dp)
                            <a type="button" href="/admin/ekskul/create/{{$dp->id}}" class="btn btn-outline-primary btn-block mb-3">{{$dp->nama}}</a>
                        @endforeach
                    </center>
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <section class="section">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table1">
                        <thead>
                            <tr>
                                <th>Nama Ekstrakurikuler</th>
                                <th>Foto</th>
                                <th>Kategori</th>
                                <th>Nama Pembimbing</th>
                                <th>Hari</th>
                                <th>Jam</th>
                                <th>Tempat</th>
                                <th>Deskripsi</th>
                                <th>Grup</th>
                                <th>Peserta</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            @foreach ($data_ekskul as $row)
                                <tr>
                                    <td>{{$row->nama_ekskul}}</td>
                                    <td>
                                        <img src="{{ asset('images/'.$row->foto) }}" alt="Image" width="300" class="img-fluid">
                                    </td>
                                    <td>{{$row->kategori}}</td>
                                    <td>{{$row->nama}}</td>
                                    <td>{{$row->hari}}</td>
                                    <td>{{\Carbon\Carbon::parse($row->jam)->format('H:i')}}</td>
                                    <td>{{$row->tempat}}</td>
                                    <td>{{$row->deskripsi}}</td>
                                    <td><center>{{$row->media}}<br><b><a href="{{$row->link_grup}}" target="_blank">link</a></b></center></td>
                                    <td>
                                        <a href="/admin/ekskul/peserta/{{$row->id}}" class="btn btn-sm btn-secondary">Peserta</a>
                                    </td>
                                    <td>
                                        <a href="/admin/ekskul/edit/{{$row->id}}" class="btn btn-sm btn-warning btn-block mb-1">Edit</a>
                                        {{-- <a href="/admin/ekskul/delete/{{$row->id}}" class="btn btn-sm btn-danger btn-block">Hapus</a> --}}
                                        <button type="button" class="btn btn-sm btn-danger btn-block" data-bs-toggle="modal" data-bs-target="#modalHapus-{{$row->id}}">
                                            Hapus
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

@foreach($data_ekskul as $de)
<div class="modal modal-borderless fade" id="modalHapus-{{$de->id}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">
                    Yakin Anda ingin menghapus?
                </h5>
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <i data-feather="x"></i>
                </button>
            </div>
            <div class="modal-footer">
                <a href="/admin/ekskul/delete/{{$de->id}}" type="button" class="btn btn-danger">
                    Ya
                </a>
                <button type="button" class="btn btn-light-secondary ml-1"
                    data-bs-dismiss="modal">
                    Tidak
                </button>
            </div>
        </div>
    </div>
</div>
@endforeach

@stop