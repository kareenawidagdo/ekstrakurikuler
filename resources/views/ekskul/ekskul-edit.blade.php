@extends('layout.admin')

@section('title', 'Edit Ekstrakurikuler')

@section('ekskul','active')

@section('konten')

<div class="page-heading">
                <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last mb-3">
                            <h3>Edit Ekstrakurikuler</h3>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-lg col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="/admin/ekskul/update/{{$data_ekskul->id}}" method="POST" class="form form-horizontal" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <label>Nama</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" class="form-control"
                                                            name="nama" value="{{$data_ekskul->nama_ekskul}}">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Foto</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="file" id="foto_id" class="form-control"
                                                            name="foto">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Kategori</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <select name="kategori" class="form-select">
                                                            <option value="Olahraga" @if($data_ekskul->kategori=='Olahraga') selected @endif>Olahraga</option>
                                                            <option value="Seni Bela diri" @if($data_ekskul->kategori=='Seni Bela diri') selected @endif>Seni Bela diri</option>
                                                            <option value="Keagamaan" @if($data_ekskul->kategori=='Keagamaan') selected @endif>Keagamaan</option>
                                                            <option value="Kesenian" @if($data_ekskul->kategori=='Kesenian') selected @endif>Kesenian</option>
                                                            <option value="Akademik" @if($data_ekskul->kategori=='Akademik') selected @endif>Akademik</option>
                                                            <option value="SCC" @if($data_ekskul->kategori=='SCC') selected @endif>SCC</option>
                                                            <option value="Krida" @if($data_ekskul->kategori=='Krida') selected @endif>Krida</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Hari</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <select name="hari" class="form-select">
                                                            <option value="Senin" @if($data_ekskul->hari=='Senin') selected @endif>Senin</option>
                                                            <option value="Selasa" @if($data_ekskul->hari=='Selasa') selected @endif>Selasa</option>
                                                            <option value="Rabu" @if($data_ekskul->hari=='Rabu') selected @endif>Rabu</option>
                                                            <option value="Kamis" @if($data_ekskul->hari=='Kamis') selected @endif>Kamis</option>
                                                            <option value="Jumat" @if($data_ekskul->hari=='Jumat') selected @endif>Jumat</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Jam</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" id="first-name" class="form-control"
                                                            name="jam" value="{{$data_ekskul->jam}}">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Tempat</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" id="first-name" class="form-control"
                                                            name="tempat" value="{{$data_ekskul->tempat}}">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Deskripsi</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <textarea class="form-control"
                                                            name="desc" rows="3">{{$data_ekskul->deskripsi}}</textarea>
                                                    </div>
                                                    <small class="mt-3 mb-1"><b>Grup </b><span style="color: red;">*</span></small>
                                                    <hr>
                                                    <div class="col-md-1">
                                                        <label>Media</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" id="first-name" class="form-control"
                                                            name="media" value="{{$data_ekskul->media}}">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Link Grup</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" id="first-name" class="form-control"
                                                            name="link" value="{{$data_ekskul->link_grup}}">
                                                    </div>
                                                    <div class="col-sm-12 d-flex justify-content-end">
                                                        <button type="submit"
                                                            class="btn btn-primary me-1 mb-1">Submit</button>
                                                        <button type="reset"
                                                            class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                    </div>

                                                    
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic Horizontal form layout section end -->
            </div>
@stop