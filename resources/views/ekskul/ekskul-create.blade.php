@extends('layout.admin')

@section('title', 'Tambah Ekstrakurikuler')

@section('ekskul','active')

@section('konten')

<div class="page-heading">
                <div class="page-title">
                    <div class="row">
                        <div class="col-12 col-md-6 order-md-1 order-last mb-3">
                            <h3>Tambah Ekstrakurikuler</h3>
                        </div>
                        <div class="col-12 col-md-6 order-md-2 order-first">
                            <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                                <ol class="breadcrumb">
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>

                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row match-height">
                        <div class="col-lg col-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="/admin/ekskul/save" method="POST" enctype="multipart/form-data" class="form form-horizontal">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <input type="hidden" name="id_pembimbing" value="{{$id}}">
                                                    <div class="col-md-1">
                                                        <label>Nama</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" id="first-name" class="form-control"
                                                            name="nama">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Foto</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="file" id="foto_id" class="form-control"
                                                            name="foto">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Kategori</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <select name="kategori" class="form-select">
                                                            <option value="" selected disabled></option>
                                                            <option value="Olahraga">Olahraga</option>
                                                            <option value="Seni Bela diri">Seni Bela diri</option>
                                                            <option value="Keagamaan">Keagamaan</option>
                                                            <option value="Kesenian">Kesenian</option>
                                                            <option value="Akademik">Akademik</option>
                                                            <option value="SCC">SCC</option>
                                                            <option value="Krida">Krida</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Hari</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <select name="hari" class="form-select">
                                                            <option value="" selected disabled></option>
                                                            <option value="Senin">Senin</option>
                                                            <option value="Selasa">Selasa</option>
                                                            <option value="Rabu">Rabu</option>
                                                            <option value="Kamis">Kamis</option>
                                                            <option value="Jumat">Jumat</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Jam</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="time" id="first-name" class="form-control"
                                                            name="jam">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Tempat</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" id="first-name" class="form-control"
                                                            name="tempat">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Deskripsi</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <textarea class="form-control"
                                                            name="desc" rows="3"></textarea>
                                                    </div>
                                                    <small class="mt-3 mb-1"><b>Grup </b><span style="color: red;">*</span></small>
                                                    <hr>
                                                    <div class="col-md-1">
                                                        <label>Media</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" id="first-name" class="form-control"
                                                            name="media">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Link Grup</label>
                                                    </div>
                                                    <div class="col-md-11 form-group">
                                                        <input type="text" id="first-name" class="form-control"
                                                            name="link">
                                                    </div>
                                                    <div class="col-sm-12 d-flex justify-content-end">
                                                        <button type="submit"
                                                            class="btn btn-primary me-1 mb-1">Submit</button>
                                                        <button type="reset"
                                                            class="btn btn-light-secondary me-1 mb-1">Reset</button>
                                                    </div>

                                                    
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic Horizontal form layout section end -->
            </div>
@stop