<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEkskulTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ekskul', function (Blueprint $table) {
            $table->id();
            $table->string('id_pembimbing', 50);
            $table->string('nama_ekskul');
            $table->string('foto')->default('default-ekskul.png')->nullable();
            $table->string('kategori')->nullable();
            $table->string('hari', 50);
            $table->time('jam');
            $table->text('tempat')->nullable();
            $table->text('deskripsi')->nullable();
            $table->string('slug')->nullable();
            $table->string('media')->nullable();
            $table->string('link_grup')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ekskul');
    }
}
