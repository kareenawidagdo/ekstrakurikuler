<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $pass = bcrypt('smajacschool');

            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password')->default($pass);
            $table->string('class', 50)->nullable();
            $table->string('photo')->default('default-profile.png');
            $table->text('description')->nullable();
            $table->string('account_status', 5)->default('2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
