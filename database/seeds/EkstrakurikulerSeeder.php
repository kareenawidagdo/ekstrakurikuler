<?php

use Illuminate\Database\Seeder;
use App\User;

class EkstrakurikulerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'name' => 'Admin',
            'email' => 'admin@jacschool.com',
            'password' => bcrypt('admin123'),
            'account_status' => '1'
        ];
        
        User::insert($user);
    }
}
