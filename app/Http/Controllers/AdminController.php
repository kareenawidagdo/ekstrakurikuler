<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ekskul;
use App\User;
use App\Peserta;
use App\Pembimbing;

class AdminController extends Controller
{
    public function index() {
        $data_ekskul = Ekskul::all();
        $data_pembimbing = Pembimbing::all();

        return view('admin.beranda', compact('data_ekskul','data_pembimbing'));
    }

    public function credit(){
        return view('layout.credits');
    }
}
