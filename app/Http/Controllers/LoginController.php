<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    public function index()
    {
        return view('layout.login');
    }

    public function store(Request $request)
    {
        $validate_admin = User::where('email', $request->input('email'))
                                ->first();

        if($validate_admin){
            if(Hash::check($request->input('password'), $validate_admin->password)){
                Auth::loginUsingId($validate_admin->id);
                $request->session()->put('id', $validate_admin->id);
                if($validate_admin->account_status == '1'){
                    return redirect(url('admin/beranda'));
                }
                if($validate_admin->account_status  == '2'){
                    return redirect(url('siswa/beranda/'.$validate_admin->id));
                }
            } else{
                Session::flash('gagal','Oppsss, Email atau Password salah');
                return redirect('/')
                    ->with('account_status', 3);
            }

        }
        else{
            Session::flash('gagal','Oppsss, Email atau Password salah');
            return redirect('/login')
            ->with('account_status', 3);
        }
    }

    function logout(Request $request){
        Auth::logout();
        $request->session()->forget('id');
        
        return redirect('/');
    }
}