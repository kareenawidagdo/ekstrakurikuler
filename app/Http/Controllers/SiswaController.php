<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Session;
use App\Ekskul;
use App\User;
use App\Peserta;
use App\Pembimbing;

class SiswaController extends Controller
{
    public function index($id){
        $data_user = User::find($id);
        $data_ekskul = Ekskul::all();
        return view('siswa.beranda', compact('data_user','data_ekskul'));
    }

    public function ekskul($slug, $id){
        $data_user = User::find($id);
        $data_ekskul = Ekskul::where('slug', $slug)
                ->leftjoin('pembimbing','pembimbing.id','=','ekskul.id_pembimbing')
                ->select('pembimbing.*','ekskul.*')
                ->first();
        
        return view('siswa.detail-ekskul', compact('data_user','data_ekskul'));
    }

    public function join($id, $id2, Request $request){
        $data_ekskul = Ekskul::find($id);
        $data_user = User::find($id2);

        $data_peserta = Peserta::create([
            "id_user"=>$id2,
            "nama"=>$data_user->name,
            "email"=>$data_user->email,
            "no_hp"=>$request->input("hp"),
            "kelas"=>$data_user->class,
            "id_ekskul"=>$id,
            "created_at"=>now(),
        ]);

        if($data_peserta){
            Session::flash('sukses','Sukses Menyimpan Data');
            return redirect(url('siswa/join/peserta/'.$data_ekskul->slug.'/'.$id2));
        } else {
            Session::flash('gagal','ERROR');
            return redirect(url()->previous());
        }
    }

    public function peserta($slug, $id){
        $data_ekskul = Ekskul::where('slug', $slug)
                ->leftjoin('pembimbing','pembimbing.id','=','ekskul.id_pembimbing')
                ->first();
        $data_user = User::find($id);

        return view('siswa.join-ekskul', compact('data_ekskul','data_user'));
    }

    public function user($id){
        $data_user = User::find($id);
        $data_ekskul = Peserta::where('id_user', $id)
                ->leftjoin('ekskul','ekskul.id','=','peserta.id_ekskul')
                ->get();
        
        return view('siswa.ekskul', compact('data_user','data_ekskul'));
    }

    public function ekskul_user($slug, $id){
        $data_user = User::find($id);
        $data_ekskul = Ekskul::where('slug', $slug)
                ->leftjoin('pembimbing','pembimbing.id','=','ekskul.id_pembimbing')
                ->select('pembimbing.*','ekskul.*')
                ->first();
        
        return view('siswa.ekskul-user', compact('data_user','data_ekskul'));
    }

    public function keluar($id, $id2){
        $data_peserta = Peserta::where('id_ekskul', $id)
                ->first();
        $data_peserta = Peserta::where('id_user', $id2)
                ->first();
        $data_peserta->delete();

        if($data_peserta){
            Session::flash('sukses','Sukses Menghapus Data');
            return redirect(url('siswa/ekstrakurikuler/'.$id2));
        }else{
            Session::flash('gagal','Gagal Menghapus Data');
            return redirect(url('siswa/ekstrakurikuler/'.$id2));
        }
    }

    public function profile($id){
        $data_user = User::find($id);

        return view('siswa.profile', compact('data_user'));
    }

    public function edit(Request $request, $id){
        $data_user = User::find($id);
        $data_user->name = $request->input('nama');
        $data_user->description = $request->input('bio');
        $data_user->save();

        {
            if($data_user){
                Session::flash('sukses','Sukses Update Data');
                return redirect(url('siswa/profile/'.$id));
            }else{
                Session::flash('gagal','Gagal Update Data');
                return redirect(url('siswa/profile/'.$id));
            }
        }
    }

    public function change(Request $request, $id) {
        $data_user = User::find($id);
        if(Hash::check($request->input('current_password'), $data_user->password)) {
            $this->validate($request, [
                'password' => 'min:6',
                'confirm_password' => 'required_with:password|same:password|min:6',
            ]);
            if($request->input('password') == $request->input('confirm_password')) {
                $password = $request->input('password');
                $pass = bcrypt($password);

                $data_user -> password = $pass;
                $data_user -> save();

                Session::flash('sukses','Password telah diubah.');
                return redirect(url('siswa/profile/'.$data_user->id));
            }
            elseif($request->input('password') != $request->input('confirm_password')) {
                Session::flash('gagal','Password salah.');
                return redirect(url('siswa/profile/'.$data_user->id));
            }
        } else {
            Session::flash('gagal','Password salah.');
            return redirect(url('siswa/profile/'.$data_user->id));
        }
    }

    public function bantuan($id){
        $data_user = User::find($id);

        return view('siswa.bantuan', compact('data_user'));
    }

    public function ganti(Request $request, $id){
        $data_user = User::find($id);

        $imgName = $request->foto->getClientOriginalName() . '-' . time() . '.' . $request->foto->extension();
        $request->foto->move(public_path('images'), $imgName);

        File::delete(public_path('images/'.$data_user->photo));

        $data_user -> photo = $imgName;
        $data_user -> save();
        return redirect(url("siswa/profile/".$data_user->id));
    }

    public function hapus($id){
        $data_user = User::find($id);
        $data_user -> photo = "default-profile.png";
        $data_user -> save();
        return redirect(url("siswa/profile/".$data_user->id));
    }
}
