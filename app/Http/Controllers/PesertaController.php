<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Session;
use App\Ekskul;
use App\User;
use App\Peserta;
use App\Pembimbing;

class PesertaController extends Controller
{
    public function index(Request $request){
        $search = $request->input('search');
        $btn = $request->input('btn');
        if($btn == "1") {
            $data_user = User::all();
        } else {
            $data_user = User::all();
        }

        return view('peserta.peserta-list')
                ->with('data_user',$data_user);
    }

    public function create(){
        return view('peserta.peserta-create');
    }

    public function save(Request $request){
        $data_user = User::create([
            "name"=>$request->input('name'),
            "email"=>$request->input('email'),
            "class"=>$request->input('kelas'),
            "created_at"=>now(),
        ]);
        if($data_user){
            Session::flash('sukses','Sukses Menyimpan Data');
            return redirect(url('admin/siswa/akun-baru/'.$data_user->id));
        }else{
            Session::flash('gagal','ERROR');
            return redirect();
        }
    }

    public function delete($id, Request $request){
        $data_peserta = Peserta::find($id);
        $data_peserta->delete();

        if($data_peserta){
            Session::flash('sukses','Sukses Menghapus Data');
            return redirect(url('admin/siswa'));
        }else{
            Session::flash('gagal','Gagal Menghapus Data');
            return redirect(url('admin/siswa'));
        }
    }

    public function ekskul($id){
        $data_user = User::find($id);
        $data_ekskul = Peserta::where('id_user', $id)
                ->join('ekskul','ekskul.id','=','peserta.id_ekskul')
                ->join('pembimbing','ekskul.id_pembimbing','=','pembimbing.id')
                ->get();

        return view('peserta.peserta-ekskul', compact('data_user','data_ekskul'));
    }

    public function new($id){
        $data_user = User::find($id);
        return view('peserta.akun-baru', compact('data_user'));
    }

    public function ekskul_delete($id, $id2){
        $data_peserta = Peserta::where('id_ekskul', $id)
                ->first();
        $data_peserta = Peserta::where('id_user', $id2)
                ->first();
        $data_peserta->delete();

        if($data_peserta){
            Session::flash('sukses','Sukses Menghapus Data');
            return redirect(url('admin/siswa/ekskul/'.$data_peserta->id_user));
        }else{
            Session::flash('gagal','Gagal Menghapus Data');
            return redirect(url('admin/siswa/ekskul/'.$data_peserta->id_user));
        }
    }
}
