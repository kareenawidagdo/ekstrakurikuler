<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Session;
use App\Ekskul;
use App\User;
use App\Peserta;
use App\Pembimbing;

class EkskulController extends Controller
{
    public function index(Request $request){
        $data_pembimbing = Pembimbing::all();
        $data_peserta = Peserta::all();
        $search = $request->input('search');
        $btn = $request->input('btn');
        if($btn == "1") {
            $data_ekskul = DB::table('ekskul')
                                ->leftjoin('pembimbing','pembimbing.id','=','ekskul.id_pembimbing')
                                ->select('pembimbing.*','ekskul.*')
                                ->get();
        } else {
            $data_ekskul = DB::table('ekskul')
                                ->leftjoin('pembimbing','pembimbing.id','=','ekskul.id_pembimbing')
                                ->select('pembimbing.*','ekskul.*')
                                ->get();
        }

        return view('ekskul.ekskul-list')
                ->with('data_pembimbing',$data_pembimbing)
                ->with('data_peserta',$data_peserta)
                ->with('data_ekskul',$data_ekskul);
    }

    public function create($id){
        return view('ekskul.ekskul-create')
            ->with('id',$id);
    }

    function save(Request $request){
        if($request->foto != ""){
            $img = $request->foto->getClientOriginalName() . '-' . time() . '.' . $request->foto->extension();
            $request->foto->move(public_path('images'), $img);
        } else {
            $img = "default-ekskul.png";
        }
        $slug = Str::slug($request->nama);

        $data_ekskul = Ekskul::create([
            "id_pembimbing"=>$request->input('id_pembimbing'),
            "nama_ekskul"=>$request->input('nama'),
            "foto"=>$img,
            "kategori"=>$request->input('kategori'),
            "hari"=>$request->input('hari'),
            "jam"=>$request->input('jam'),
            "tempat"=>$request->input('tempat'),
            "deskripsi"=>$request->input('desc'),
            "slug"=>$slug,
            "media"=>$request->input('media'),
            "link_grup"=>$request->input('link'),
            "created_at"=>now(),
        ]);
        if($data_ekskul){
            Session::flash('sukses','Sukses Menyimpan Data');
            return redirect(url('admin/ekskul'))
                    ->with("data_ekskul",$data_ekskul);
        }else{
            Session::flash('gagal','ERROR');
            return redirect(url('admin/ekskul'));
        }
    }

    public function edit($id, Request $request){
        $data_ekskul = Ekskul::find($id);
        return view('ekskul.ekskul-edit')
            ->with('data_ekskul', $data_ekskul);
    }

    public function update($id, Request $request){
        $data_ekskul = Ekskul::find($id);

        if($request->foto != ""){
            $img = $request->foto->getClientOriginalName() . '-' . time() . '.' . $request->foto->extension();
            $request->foto->move(public_path('images'), $img);

            File::delete(public_path('images/'.$data_ekskul->foto));
        } else {
            $img = $data_ekskul->foto;
        }
        $slug = Str::slug($request->nama);
        
        $data_ekskul->nama_ekskul = $request->input('nama');
        $data_ekskul->foto = $img;
        $data_ekskul->kategori = $request->input('kategori');
        $data_ekskul->hari = $request->input('hari');
        $data_ekskul->jam = $request->input('jam');
        $data_ekskul->tempat = $request->input('tempat');
        $data_ekskul->deskripsi = $request->input('desc');
        $data_ekskul->slug = $slug;
        $data_ekskul->media = $request->input('media');
        $data_ekskul->link_grup = $request->input('link');
        $data_ekskul->updated_at = now();
        $data_ekskul->save();

        {
            if($data_ekskul){
                Session::flash('sukses','Sukses Update Data');
                return redirect(url('admin/ekskul'));
            }else{
                Session::flash('gagal','Gagal Update Data');
                return redirect(url('admin/ekskul'));
            }
        }
    }

    public function delete($id, Request $request){
        $data_ekskul = Ekskul::find($id);
        $data_ekskul->delete();

        if($data_ekskul){
            Session::flash('sukses','Sukses Menghapus Data');
            return redirect(url('admin/ekskul'));
        }else{
            Session::flash('gagal','Gagal Menghapus Data');
            return redirect(url('admin/ekskul'));
        }
    }

    public function new(){
        return view('ekskul.ekskul-pembimbing');
    }

    public function new_save(Request $request){
        $data_pembimbing = Pembimbing::create([
            "nama"=>$request->input("nama"),
            "email"=>$request->input("email"),
            "no_hp"=>$request->input("hp"),
            "created_at"=>now(),
        ]);

        if($data_pembimbing){
            Session::flash('sukses','Sukses Menyimpan Data');
            return redirect(url('admin/ekskul/create/'.$data_pembimbing->id))
                    ->with("data_pembimbing",$data_pembimbing);
        }else{
            Session::flash('gagal','ERROR');
            return redirect(url('admin/ekskul/pembimbing-baru'));
        }
    }

    public function peserta($id){
        $data_ekskul = Ekskul::find($id);
        $data_siswa = Peserta::where('id_ekskul', $id)->get();

        return view('ekskul.ekskul-peserta', compact('data_ekskul','data_siswa'));
    }

    public function peserta_delete($id){
        $data_siswa = Peserta::find($id);
        $data_siswa->delete();

        if($data_siswa){
            Session::flash('sukses','Sukses Menghapus Data');
            return redirect(url()->previous());
        }else{
            Session::flash('gagal','Gagal Menghapus Data');
            return redirect(url()->previous());
        }
    }
}
