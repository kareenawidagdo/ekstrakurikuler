<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembimbing;
use App\Ekskul;
use App\Peserta;
use App\User;
use Session;

class PembimbingController extends Controller
{
    function list(Request $request){
        $search = $request->input('search');
        $btn = $request->input('btn');
        if($btn == "1") {
            $data_pembimbing = Pembimbing::all();
        } else {
            $data_pembimbing = Pembimbing::all();
        }
        return view('pembimbing.pembimbing-list')
            ->with('data_pembimbing',$data_pembimbing);
        
    }

    function create(){
        return view('pembimbing.pembimbing-create');
    }

    public function save(Request $request){
        $data_pembimbing = Pembimbing::create([
            "nama"=>$request->input("nama"),
            "email"=>$request->input("email"),
            "no_hp"=>$request->input("hp"),
            "created_at"=>now(),
        ]);

        if($data_pembimbing){
            Session::flash('sukses','Sukses Menyimpan Data');
            return redirect(url('admin/pembimbing'))
                    ->with("data_pembimbing",$data_pembimbing);
        } else {
            Session::flash('gagal','ERROR');
            return redirect(url('admin/pembimbing'));
        }
    }

    function edit($id,Request $request){
        $data_pembimbing = Pembimbing::find($id);
        return view ('pembimbing.pembimbing-edit')
            ->with('data_pembimbing',$data_pembimbing);
    }

    function update($id,Request $request){
        $data_pembimbing = Pembimbing::find($id);
        $data_pembimbing->nama = $request->input("nama");
        $data_pembimbing->email = $request->input("email");
        $data_pembimbing->no_hp = $request->input("hp");
        $data_pembimbing->save();

        {
            if($data_pembimbing){
                Session::flash('sukses','Sukses Update Data');
                return redirect(url('admin/pembimbing'));
            }else{
                Session::flash('gagal','Gagal Update Data');
                return redirect(url('admin/pembimbing'));
            }
        }
    }

    function delete($id,Request $request){
        $data_pembimbing = Pembimbing::find($id);
        $data_pembimbing -> delete();

        if($data_pembimbing){
            Session::flash('sukses','Sukses Menghapus Data');
            return redirect(url('admin/pembimbing'));
        }else{
            Session::flash('gagal','Gagal Menghapus Data');
            return redirect(url('admin/pembimbing'));
        }
    }

    public function ekskul($id){
        $data_pembimbing = Pembimbing::find($id);
        $data_ekskul = Ekskul::where('id_pembimbing', $id)->get();

        return view('pembimbing.pembimbing-ekskul', compact('data_pembimbing','data_ekskul'));
    }
}
