<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



    // Login
Route::get('/', 'LoginController@index');
Route::post('auth-login', 'LoginController@store');
Route::get('logout', 'LoginController@logout');

    // Credit
Route::get('credits','AdminController@credit');

    // Admin Routes
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function () {

    // Beranda - admin
Route::get('admin/beranda','AdminController@index');

    // Ekskul - admin
Route::get('admin/ekskul','EkskulController@index');
Route::get('admin/ekskul/create/{id}','EkskulController@create');
Route::post('admin/ekskul/save','EkskulController@save');
Route::get('admin/ekskul/edit/{id}','EkskulController@edit');
Route::post('admin/ekskul/update/{id}','EkskulController@update');
Route::get('admin/ekskul/delete/{id}','EkskulController@delete');
Route::get('admin/ekskul/pembimbing-baru','EkskulController@new');
Route::post('admin/ekskul/save-pembimbing','EkskulController@new_save');
Route::get('admin/ekskul/peserta/{id}','EkskulController@peserta');
Route::get('admin/ekskul/siswa/delete/{id}','EkskulController@peserta_delete');

    // Siswa - admin
Route::get('admin/siswa','PesertaController@index');
Route::get('admin/siswa/create','PesertaController@create');
Route::post('admin/siswa/save','PesertaController@save');
Route::get('admin/siswa/delete/{id}','PesertaController@delete');
Route::get('admin/siswa/ekskul/{id}','PesertaController@ekskul');
Route::get('admin/siswa/akun-baru/{id}','PesertaController@new');
Route::get('admin/siswa/ekskul/delete/{id}/{id2}','PesertaController@ekskul_delete');

    // Pembimbing - admin
Route::get('admin/pembimbing','PembimbingController@list');
Route::get('admin/pembimbing/create','PembimbingController@create');
Route::post('admin/pembimbing/save','PembimbingController@save');
Route::get('admin/pembimbing/edit/{id}','PembimbingController@edit');
Route::post('admin/pembimbing/update/{id}','PembimbingController@update');
Route::get('admin/pembimbing/delete/{id}','PembimbingController@delete');
Route::get('admin/pembimbing/ekskul/{id}','PembimbingController@ekskul');

});

    // Siswa
Route::group(['middleware' => 'App\Http\Middleware\SiswaMiddleware'], function () {

    // Beranda - siswa
Route::get('siswa/beranda/{id}','SiswaController@index');
Route::get('siswa/detail/{slug}/{id}','SiswaController@ekskul');
Route::post('siswa/join/{id}/{id2}','SiswaController@join');
Route::get('siswa/join/peserta/{slug}/{id}','SiswaController@peserta');
Route::get('siswa/ekstrakurikuler/{id}','SiswaController@user');
Route::get('siswa/ekstrakurikuler/{slug}/{id}','SiswaController@ekskul_user');
Route::get('siswa/keluar/{id}/{id2}','SiswaController@keluar');
Route::get('siswa/profile/{id}','SiswaController@profile');
Route::post('siswa/profile/edit/{id}','SiswaController@edit');
Route::post('siswa/profile/change-password/{id}','SiswaController@change');
Route::get('siswa/bantuan/{id}','SiswaController@bantuan');
Route::post('siswa/profile/ganti/{id}','SiswaController@ganti');
Route::get('siswa/profile/hapus/{id}','SiswaController@hapus');

});